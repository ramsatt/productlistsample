import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
declare const google: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit{
  /*map: any;
  @ViewChild('mapElement') mapElement: any;*/

  productArray = [
    {name: 'AC', value: 'AC', brands: [
        {name: 'Samsung', value: 'samsung', isSelected: false},
        {name: 'Samsung1', value: 'samsung1', isSelected: true},
        {name: 'Samsung2', value: 'samsung2', isSelected: false},
      ]},
    {name: 'FAN', value: 'FAN', brands: [
        {name: 'Crompton', value: 'Crompton', isSelected: false},
        {name: 'Crompton1', value: 'Crompton1', isSelected: false},
        {name: 'Crompton2', value: 'Crompton2', isSelected: false}
      ]
    }
  ]

  selectedProduct: any;
  selectedArray: any = [];
  selectedValues: any = [];

  constructor() {
  }

  ngAfterViewInit(): void {
    //map code here
    /*this.map = new google.maps.Map(this.mapElement.nativeElement, {
      center: { lat: 13.0827, lng: 80.2707 },
      zoom: 14,
    });*/
  }

  ngOnInit(): void {
  }

  getProductArray() {
    let pArray = this.productArray.map(product => product.name).indexOf(this.selectedProduct);

    this.selectedArray = this.productArray[pArray];
    console.log('pArray', this.selectedArray)
  }

  storeValues(event: any, brands: any) {

  }
}
